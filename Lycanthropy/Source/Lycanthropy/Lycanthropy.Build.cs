// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Lycanthropy : ModuleRules
{
	public Lycanthropy(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "UMG", "OnlineSubsystem", "OnlineSubsystemSteam", "GameplayTasks" });
	}
}
