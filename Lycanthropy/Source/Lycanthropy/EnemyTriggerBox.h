// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerBox.h"
#include "EnemyTriggerBox.generated.h"

/**
 * 
 */
UCLASS()
class LYCANTHROPY_API AEnemyTriggerBox : public ATriggerBox
{
	GENERATED_BODY()
	
public:
	AEnemyTriggerBox();
};
