// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon.h"
#include "Components/SkeletalMeshComponent.h"
#include "DrawDebugHelpers.h"

#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystem.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "TimerManager.h"
#include "Sound/SoundCue.h"
#include "Animation/AnimMontage.h"
#include "Net/UnrealNetwork.h"
#include "Components/BoxComponent.h"

#include "LycanthropyCharacter.h"

//Draw debug hit detection lines
static int32 DebugWeaponDrawing = 1;
FAutoConsoleVariableRef CVARDebugWeaponDrawing(
	TEXT("MELEE.DebugWeapons"),
	DebugWeaponDrawing,
	TEXT("Draw Debug Lines for Weapons"),
	ECVF_Cheat);

// Sets default values
AWeapon::AWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SetReplicates(true);

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	SetRootComponent(Root);

	Mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(Root);

	Mesh->OnComponentBeginOverlap.AddDynamic(this, &AWeapon::OnBeginOverlap);

	SetWeaponState(EWeaponState::WS_None);
}

/*OnBeginOverlap
Called when our weapon overlaps with something else ( in our case with another weapon)
In the case of 1H Weapons ( using Melee Weapon Shield  class ), It will bind the overlap event to the
Shield Skeletal mesh component, rather than the weapon
for weapons that use the Melee Weapon Class it will use the Weapon Mesh Skeletal mesh component
*/
void AWeapon::OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	FHitResult HitData;
	FCollisionQueryParams Params;
	FCollisionShape SphereCollision = FCollisionShape::MakeSphere(12.0f);
	FCollisionObjectQueryParams ObjParams;
	FVector WTraceStart;
	FVector WTraceEnd;
	ObjParams.AddObjectTypesToQuery(ECollisionChannel::ECC_GameTraceChannel1);
}

void AWeapon::OnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{

}

/*SetWeaponState

Sets the weapon state to the given enum
Also plays the appropiate animation montage based on the state
mostly used for changes in the blocking state
this is ALWAYS run on the server
*/
void AWeapon::SetWeaponState(EWeaponState NewWeaponState)
{
	ALycanthropyCharacter* Character = Cast<ALycanthropyCharacter>(GetOwner());
	if (Character == nullptr) return;
	EWeaponState OldWeaponState = WeaponState;
	WeaponState = NewWeaponState;

	//If our state is none, then stop tracing
	if (NewWeaponState == EWeaponState::WS_None)
	{
		bPerformTracing = false;
	}

	//Blocking handling goes here
}

/*CanSwingWeapon

returns true if the player can swing the weapon
Add the conditions that you require for the player to swing his weapon here!
By default the player must not be swinging, or blocking
*/
bool AWeapon::CanSwingWeapon()
{
	//If we are not swinging already
	if (bCurrentlySwinging == false)
	{
		return true;
	}

	else {
		return false;
	}
}

//Deal damage and play attack animation
void AWeapon::Attack()
{
	ALycanthropyCharacter* AttackingCharacter = Cast<ALycanthropyCharacter>(GetOwner());
	if (!ensure(AttackingCharacter != nullptr)) return;

	//Only run this if we are not the server
	if (GetLocalRole() != ROLE_Authority)
	{
		ServerAttack();
	}

	//Have server play animation and perform hit detection
	if (GetLocalRole() == ROLE_Authority)
	{
		if (CanSwingWeapon())
		{
			//If we come here from not swinging
			if (WeaponState == EWeaponState::WS_None)
			{
				//Set weapon state to swinging
				SetWeaponState(EWeaponState::WS_Swinging);
			}

			switch (WeaponState)
			{
			case (EWeaponState::WS_Swinging):
				//Disable tracing
				bPerformTracing = false;

				//Force it to replicate
				ForceNetUpdate();

				//if (AttackingCharacter->bIsHeavyAttacking)
				//{
				//	AttackingCharacter->MulticastPlayAnimMontage(WeaponSwingsMontage, 0.5f, "Default");
				//}
				//else
				//{
				//	AttackingCharacter->MulticastPlayAnimMontage(WeaponSwingsMontage, 1.5f, "Default");
				//}
				AttackingCharacter->MulticastPlayAnimMontage(WeaponSwingsMontage, 1.5f, "Default");

				//Emppty hit lists
				EnemiesHitOnThisSwing.Empty();
				ActorsHitOnThisSwing.Empty();

				//Set ourself as currently swinging
				bCurrentlySwinging = true;
				break;

			case EWeaponState::WS_None:
				break;
			}

			if (bCurrentlySwinging)
			{
				//play the sword swing sound
				AttackingCharacter->MulticastPlayReplicatedSound(false, SwordSwingSound);
			}
		}
	}
}

/*ToggleTracing

Toggles tracing for both the client and the server based on the role of whoever called the function
*/
void AWeapon::ToggleTracing()
{
	/*As the client*/
	if (GetLocalRole() < ROLE_Authority)
	{
		//
		if (bPerformTracing == true)
		{
			bPerformTracing = false;
			return;
		}
		if (bPerformTracing == false)
		{
			bPerformTracing = true;
			return;
		}
	}

	if (bPerformTracing == true)
	{
		bPerformTracing = false;
		return;
	}
	if (bPerformTracing == false)
	{
		bPerformTracing = true;
		return;
	}

}

/*Runs on client, plays the impact effects for the client*/
void AWeapon::ClientPlayImpactFX_Implementation(FHitTraceInfo HitInfo)
{
	PlayImpactEffects(HitInfo.FXToPlay, HitInfo.HitLocation);
}

bool AWeapon::ClientPlayImpactFX_Validate(FHitTraceInfo HitInfo)
{
	return true;
}

/*IsSwinging
returns true if the player is currently swinging, false if he is not swinging or has initiated a swing
*/
bool AWeapon::IsSwinging()
{
	if (WeaponState == EWeaponState::WS_Swinging || WeaponState == EWeaponState::WS_HeavySwing || WeaponState == EWeaponState::WS_Reversal)
	{
		return true;
	}

	return false;
}

void AWeapon::AttackEnd()
{
	//we have fully blended out and are out of the swing so set our state back to none
	if (GetLocalRole() < ROLE_Authority)
	{
		//Empty our enemy hit list
		EnemiesHitOnThisSwing.Empty();

		//Stop tracing
		bPerformTracing = false;
		return;
	}

	//Runs on server
	SetWeaponState(EWeaponState::WS_None);

	bPerformTracing = false;
	bCurrentlySwinging = false;
	//Cast<ALycanthropyCharacter>(GetOwner())->bIsHeavyAttacking = false;
	EnemiesHitOnThisSwing.Empty();
}

void AWeapon::ServerAttack_Implementation()
{
	Attack();
}

bool AWeapon::ServerAttack_Validate()
{
	return true;
}

/*CheckHurtEnemies
Checks if we have hurt this character in the current swing alredy or not
*/
bool AWeapon::CheckHurtEnemies(ACharacter* HurtChar)
{
	int i;

	i = EnemiesHitOnThisSwing.Find(HurtChar);

	if (i != INDEX_NONE)
	{
		return false;
	}
	else
	{
		EnemiesHitOnThisSwing.Add(HurtChar);
	}

	return true;
}

/*CheckHurtActors
Same as check hurt enemies, except it checks for actors
*/
bool AWeapon::CheckHurtActors(AActor* HurtActor)
{
	int i;

	i = ActorsHitOnThisSwing.Find(HurtActor); // Find the actor/pawn we hit in the array

	if (ActorsHitOnThisSwing.IsValidIndex(i))
	{

		if (ActorsHitOnThisSwing[i]->IsValidLowLevel())
		{
			return false;
		}
		else 
		{
			ActorsHitOnThisSwing.Add(HurtActor);
			return true;
		}
	}
	ActorsHitOnThisSwing.Add(HurtActor);
	return true;
}

// Called when the game starts or when spawned
void AWeapon::BeginPlay()
{
	Super::BeginPlay();
	
}

/*Tick
Handles the tracing logic for both the server and the client
*/
// Called every frame
void AWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//int i;
	FHitResult HitData(ForceInit);
	FHitResult MeshHitData(ForceInit);
	APawn* MyOwner = Cast<APawn>(GetOwner());
	ALycanthropyCharacter* Char = Cast<ALycanthropyCharacter>(MyOwner);
	FCollisionQueryParams Params;
	Params.AddIgnoredActor(MyOwner);
	//Params.bReturnPhysicalMaterial = true;
	Params.bTraceComplex = false;
	//Params.bTraceAsyncScene = true;
	float DamageToDeal = 0.0f;

	EPhysicalSurface SurfaceType = SurfaceType_Default;

	FCollisionQueryParams MeshParams;
	MeshParams.AddIgnoredActor(MyOwner);
	//MeshParams.bReturnPhysicalMaterial = true;

	//Update the start and end socket locations
	StartSockEnd = Mesh->GetSocketLocation(SocketStartName);
	EndSockEnd = Mesh->GetSocketLocation(SocketEndName);



	//If we should perform tracing and we are locally controlled
	if (bPerformTracing && MyOwner && MyOwner->IsLocallyControlled())
	{
		//Get the start and end location to make the traces from
		StartSockEnd = Mesh->GetSocketLocation(SocketStartName);
		EndSockEnd = Mesh->GetSocketLocation(SocketEndName);

		//Adjust the damage we will deal based on the weapon state
		if (WeaponState == EWeaponState::WS_Swinging || WeaponState == EWeaponState::WS_Reversal)
		{
			DamageToDeal = NormalSwingDamage;
		}

		if (WeaponState == EWeaponState::WS_HeavySwing)
		{
			DamageToDeal = HeavySwingDamage;
		}


		for (int i = 0; i < TracesPerSwing; i++)
		{
			TraceStart = FMath::Lerp(StartSockEnd, EndSockEnd, i / TracesPerSwing);
			TraceEnd = FMath::Lerp(LastFrameStartSockEnd, LastFrameEndSockEnd, i / TracesPerSwing);

			if (DebugWeaponDrawing > 0)
			{
				DrawDebugLine(GetWorld(), TraceStart, TraceEnd, FColor(255, 0, 0), false, 1, 0, 0.5);

			}

			//First, trace use a standard trace using our vectors
			if (GetWorld()->LineTraceSingleByChannel(HitData, TraceStart, TraceEnd, ECollisionChannel::ECC_Pawn, Params))
			{
				ACharacter* HitPawn = Cast<ACharacter>(HitData.GetActor());
				OnHit(HitData);
				ALycanthropyCharacter* HitCharacter = Cast<ALycanthropyCharacter>(HitPawn);
				ALycanthropyCharacter* OwnerCharacter = Cast<ALycanthropyCharacter>(GetOwner());

				//Set last hit actor, for kill count purposes
				if (HitCharacter != nullptr && OwnerCharacter != nullptr)
				{
					HitCharacter->LastContact = OwnerCharacter;
				}

				//AActor* HitActor = HitData.GetActor();
				//if we hit a character, and that character is of a different character type
				if (HitPawn && (HitCharacter->CharacterType != OwnerCharacter->CharacterType))
				{
					//Do a component level trace, to get the more accurate hit location
					if (HitPawn->GetMesh()->LineTraceComponent(MeshHitData, TraceStart, TraceEnd, MeshParams))
					{

						// Make sure we grab the pawn we have hit from the trace
						if (HitPawn == MeshHitData.Actor)
						{


							//SurfaceType = UPhysicalMaterial::DetermineSurfaceType(HitData.PhysMaterial.Get());

							//Check if this pawn was hit previously on this swing ( if not then continue )
							if (CheckHurtEnemies(HitPawn) == true)
							{
								AActor* HitActor = MeshHitData.GetActor();

								if (MyOwner && MyOwner->IsLocallyControlled() && GetNetMode() == NM_Client)
								{
									// If we are a client trying to hit something controlled by the server
									if (MeshHitData.GetActor() && MeshHitData.GetActor()->GetRemoteRole() == ROLE_Authority)
									{
										// Notify the server of our hit and apply damage
										ServerDealDamage(MyOwner, HitActor, DamageToDeal, MeshHitData);
									}
								}

								//If we are the server
								if (GetLocalRole() == ROLE_Authority && MyOwner->GetNetMode() != NM_Client)
								{
									//Deal damage
									UGameplayStatics::ApplyPointDamage(HitActor, DamageToDeal, FVector(0, 0, 0), MeshHitData, MyOwner->GetInstigatorController(), MyOwner, DamageType);
									Char->MulticastPlayReplicatedSound(false, ImpactFleshSound);
								}

								APlayerController* PC = Cast<APlayerController>(MyOwner->GetController());

								if (PC)
								{
									//Play the hit camera shake
									//PC->ClientPlayCameraShake(SwingHitCameraShake);
								}

								//Play the impact effects on the exact point our trace hit
								PlayImpactEffects(EFXTypes::FXT_HitFlesh, MeshHitData.ImpactPoint);

								if (DebugWeaponDrawing > 0)
								{
									DrawDebugSphere(GetWorld(), MeshHitData.ImpactPoint, 12, 8, FColor::Green, false, 5.0f);
								}
								//Replicate the impact effects for the rest of the clients
								if (GetLocalRole() == ROLE_Authority)
								{
									HitTraceInfo.HitLocation = MeshHitData.ImpactPoint;
									//HitTraceInfo.SurfaceType = SurfaceType;
									HitTraceInfo.FXToPlay = EFXTypes::FXT_HitFlesh;
								}

							}

						}

					}


				}
				//If we have hit something esle
				else
				{
					//And we dident hit it on this swing preivously
					//if (CheckHurtActors(HitActor) == true)
					//{
						//then ricochet
						//Ricochet(HitData, false);
					//}
				}
			}
		}


	}
	//Update the last frame vectors
	LastFrameStartSockEnd = StartSockEnd;
	LastFrameEndSockEnd = EndSockEnd;


	//if (bAutoSwing)
	//{
	//	SwingWeapon();
	//}


}

void AWeapon::DealDamage(APawn* DamageInstigator, AActor* ActorToDmg, float Damage, FHitResult HitInfo)
{
	//FString DamagerName = DamageInstigator->GetActorLabel();
	//FString DamagedActorName = ActorToDmg->GetActorLabel();
	//FString DamageAmount = FString::SanitizeFloat(Damage);
	//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, DamagerName);
	//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, DamagedActorName);
	//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, DamageAmount);

	FHitResult Null;
	ALycanthropyCharacter* Char = Cast<ALycanthropyCharacter>(DamageInstigator);

	//EPhysicalSurface SurfaceTyp = UPhysicalMaterial::DetermineSurfaceType(HitInfo.PhysMaterial.Get());

	//Apply damage to the actor we have hit
	UGameplayStatics::ApplyPointDamage(ActorToDmg, Damage, HitInfo.Location, HitInfo, DamageInstigator->GetInstigatorController(), DamageInstigator, DamageType);
	//play the impact effects locally
	PlayImpactEffects(EFXTypes::FXT_HitFlesh, HitInfo.ImpactPoint);
	//replicate
	HitTraceInfo.HitLocation = HitInfo.ImpactPoint;
	//Char->ImpactLocation = HitInfo.ImpactPoint;
	//HitTraceInfo.SurfaceType = SurfaceTyp;
	HitTraceInfo.FXToPlay = EFXTypes::FXT_HitFlesh;

	//replicate the flesh hit sound
	Char->MulticastPlayReplicatedSound(false, ImpactFleshSound);
}

void AWeapon::ServerDealDamage_Implementation(APawn* DamageInstigator, AActor* ActorToDmg, float Damage, FHitResult HitInfo)
{
	DealDamage(DamageInstigator, ActorToDmg, Damage, HitInfo);
}

bool AWeapon::ServerDealDamage_Validate(APawn* DamageInstigator, AActor* ActorToDmg, float Damage, FHitResult HitInfo)
{
	return true;
}

void AWeapon::OnRep_HitTrace()
{
	PlayImpactEffects(HitTraceInfo.FXToPlay, HitTraceInfo.HitLocation);
}

void AWeapon::PlayImpactEffects(EFXTypes FXToPlay, FVector ImpactLoc)
{
	//Play the appropiate particle system based on the EFXTypes given
	switch (FXToPlay)
	{

	case EFXTypes::FXT_HitFlesh:
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactFlesh, ImpactLoc, ImpactLoc.Rotation(), true);
		break;
	}

	case EFXTypes::FXT_HitWorld:
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactWorldFX, ImpactLoc, ImpactLoc.Rotation(), true);
		break;
	}

	case EFXTypes::FXT_HitSword:
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactSwordFX, ImpactLoc, ImpactLoc.Rotation(), true);
		break;
	}

	case EFXTypes::FXT_HitAbsorb:
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactSwordFX, ImpactLoc, ImpactLoc.Rotation(), true);
		break;
	}

	}


}

void AWeapon::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(AWeapon, HitTraceInfo, COND_SkipOwner);
	DOREPLIFETIME_CONDITION(AWeapon, bCurrentlySwinging, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(AWeapon, WeaponState, COND_OwnerOnly);
}