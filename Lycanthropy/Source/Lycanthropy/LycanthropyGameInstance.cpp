// Fill out your copyright notice in the Description page of Project Settings.


#include "LycanthropyGameInstance.h"

#include "Engine/Engine.h"
#include "UObject/ConstructorHelpers.h"
#include "Blueprint/UserWidget.h"
#include "OnlineSubsystem.h"
#include "OnlineSessionSettings.h"

#include "MenuSystem/MainMenu.h"
#include "MenuSystem/MenuWidget.h"
#include "MenuSystem/UIWidget.h"

const static FName SESSION_NAME = NAME_GameSession;	//NAME_GameSession is an Unreal enum for session name which will work across any version of Unreal
const static FName SERVER_NAME_SETTINGS_KEY = TEXT("ServerName");

//Constructor
ULycanthropyGameInstance::ULycanthropyGameInstance(const FObjectInitializer& ObjectInitializer)
{
	//Used for finding a Blueprint class for reference. Must be placed in the constructor.
	ConstructorHelpers::FClassFinder<UUserWidget> MenuBPClass(TEXT("/Game/MenuSystem/WBP_MainMenu"));
	if (!ensure(MenuBPClass.Class != nullptr)) return;
	MenuClass = MenuBPClass.Class;

	//Used for finding a Blueprint class for reference. Must be placed in the constructor.
	ConstructorHelpers::FClassFinder<UUserWidget> InGameMenuBPClass(TEXT("/Game/MenuSystem/WBP_InGameMenu"));
	if (!ensure(InGameMenuBPClass.Class != nullptr)) return;
	InGameMenuClass = InGameMenuBPClass.Class;

	//Used for finding a Blueprint class for reference. Must be placed in the constructor.
	ConstructorHelpers::FClassFinder<UUserWidget> EndgameMenuBPClass(TEXT("/Game/MenuSystem/WBP_EndgameMenu"));
	if (!ensure(EndgameMenuBPClass.Class != nullptr)) return;
	EndgameMenuClass = EndgameMenuBPClass.Class;

	//Used for finding a Blueprint class for reference. Must be placed in the constructor.
	ConstructorHelpers::FClassFinder<UUserWidget> EndgameMenuDefeatBPClass(TEXT("/Game/MenuSystem/WBP_EndgameMenuDefeat"));
	if (!ensure(EndgameMenuDefeatBPClass.Class != nullptr)) return;
	EndgameMenuDefeatClass = EndgameMenuDefeatBPClass.Class;

	//Used for finding a Blueprint class for reference. Must be placed in the constructor.
	ConstructorHelpers::FClassFinder<UUserWidget> UIBPClass(TEXT("/Game/MenuSystem/WBP_UI"));
	if (!ensure(UIBPClass.Class != nullptr)) return;
	UIClass = UIBPClass.Class;
}

void ULycanthropyGameInstance::Init()
{
	//
	IOnlineSubsystem* Subsystem = IOnlineSubsystem::Get();
	if (Subsystem != nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("Found subsytem %s"), *Subsystem->GetSubsystemName().ToString());

		//IOnlineSessionPtr is a shared pointer. Shared pointers are checked using .IsValid() rather than != nullptr.
		SessionInterface = Subsystem->GetSessionInterface();
		if (SessionInterface.IsValid())
		{
			//Create delegates for create, destroy, and find session
			SessionInterface->OnCreateSessionCompleteDelegates.AddUObject(this, &ULycanthropyGameInstance::OnCreateSessionComplete);
			SessionInterface->OnDestroySessionCompleteDelegates.AddUObject(this, &ULycanthropyGameInstance::OnDestroySessionComplete);
			SessionInterface->OnFindSessionsCompleteDelegates.AddUObject(this, &ULycanthropyGameInstance::OnFindSessionsComplete);
			SessionInterface->OnJoinSessionCompleteDelegates.AddUObject(this, &ULycanthropyGameInstance::OnJoinSessionComplete);
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Found null subsytem"));
	}

	//Log Message to be displayed to console
	UE_LOG(LogTemp, Warning, TEXT("Found class %s"), *MenuClass->GetName());

	//Handler for network failure (i.e. the host leaves the server)
	if (GEngine != nullptr)
	{
		GEngine->OnNetworkFailure().AddUObject(this, &ULycanthropyGameInstance::OnNetworkFailure);
	}
}

void ULycanthropyGameInstance::LoadMenuWidget()
{
	//Get menu class from Blueprint
	if (!ensure(MenuClass != nullptr)) return;	//Protection from pointer being null causing editor crash

	Menu = CreateWidget<UMainMenu>(this, MenuClass);
	if (!ensure(MenuClass != nullptr)) return;	//Protection from pointer being null causing editor crash

	//
	Menu->Setup();
	Menu->SetMenuInterface(this);
}

void ULycanthropyGameInstance::InGameLoadMenu()
{
	//Get menu class from Blueprint
	if (!ensure(InGameMenuClass != nullptr)) return;	//Protection from pointer being null causing editor crash
	UMenuWidget* InGameMenu = CreateWidget<UMenuWidget>(this, InGameMenuClass);
	if (!ensure(MenuClass != nullptr)) return;	//Protection from pointer being null causing editor crash

	//
	InGameMenu->Setup();
	InGameMenu->SetMenuInterface(this);
}

void ULycanthropyGameInstance::EndgameLoadMenu()
{
	//Get menu class from Blueprint
	if (!ensure(EndgameMenuClass != nullptr)) return;	//Protection from pointer being null causing editor crash
	UMenuWidget* EndgameMenu = CreateWidget<UMenuWidget>(this, EndgameMenuClass);

	//
	EndgameMenu->Setup();
	EndgameMenu->SetMenuInterface(this);
}

void ULycanthropyGameInstance::EndgameLoadMenuDefeat()
{
	//Get menu class from Blueprint
	if (!ensure(EndgameMenuDefeatClass != nullptr)) return;	//Protection from pointer being null causing editor crash
	UMenuWidget* EndgameMenuDefeat = CreateWidget<UMenuWidget>(this, EndgameMenuDefeatClass);

	//
	EndgameMenuDefeat->Setup();
	EndgameMenuDefeat->SetMenuInterface(this);
}

void ULycanthropyGameInstance::ShowUI()
{
	//Get menu class from Blueprint
	if (!ensure(UIClass != nullptr)) return;	//Protection from pointer being null causing editor crash
	UUIWidget* UI = CreateWidget<UUIWidget>(this, UIClass);

	//Create UI
	UI->Setup();
	//GetWorld()->GetFirstLocalPlayerFromController();
	//UI->SetUIInterface(this);
}

void ULycanthropyGameInstance::KillUI()
{
	//Get menu class from Blueprint
	if (!ensure(UIClass != nullptr)) return;	//Protection from pointer being null causing editor crash
	UUIWidget* UI = CreateWidget<UUIWidget>(this, UIClass);

	//Tear down UI
	UI->Teardown();
}

void ULycanthropyGameInstance::Host(FString ServerName)
{
	DesiredServerName = ServerName;
	if (SessionInterface.IsValid()) {
		auto ExistingSession = SessionInterface->GetNamedSession(SESSION_NAME);
		if (ExistingSession != nullptr)
		{
			SessionInterface->DestroySession(SESSION_NAME);
		}
		else
		{
			CreateSession();
		}
	}
}

void ULycanthropyGameInstance::OnCreateSessionComplete(FName SessionName, bool Success)
{
	if (!Success)
	{
		UE_LOG(LogTemp, Warning, TEXT("Could not create session"));
		return;
	}
	if (Menu != nullptr)
	{
		Menu->Teardown();
	}

	//On-screen debug message
	UEngine* Engine = GetEngine();	//Get engine pointer
	if (!ensure(Engine != nullptr)) return;	//Protection from engine pointer being null causing editor crash

	Engine->AddOnScreenDebugMessage(0, 5, FColor::Green, TEXT("Hosting"));

	//Server Travel (All player controllers in the server move to another level on the server. Creates a server if one does not already exist.)
	UWorld* World = GetWorld();	//Get world object
	if (!ensure(World != nullptr)) return;	//Protection from world pointer being null causing editor crash

	World->ServerTravel("/Game/Maps/Lobby?listen");
}

void ULycanthropyGameInstance::OnDestroySessionComplete(FName SessionName, bool Success)
{
	if (Success)
	{
		CreateSession();
	}
}

//Load main menu if network fails (i.e. host leaves server)
void ULycanthropyGameInstance::OnNetworkFailure(UWorld* World, UNetDriver* NetDriver, ENetworkFailure::Type FailureType, const FString& ErrorString)
{
	LoadMainMenu();
}

void ULycanthropyGameInstance::RefreshServerList()
{
	//Search for sessions
	SessionSearch = MakeShareable(new FOnlineSessionSearch());	//The new keyword means to make something on the heap, instead of the stack
	if (SessionSearch.IsValid())
	{
		//SessionSearch->bIsLanQuery = true;
		SessionSearch->MaxSearchResults = 100;
		SessionSearch->QuerySettings.Set(SEARCH_PRESENCE, true, EOnlineComparisonOp::Equals);	//Allows Steam search presence
		UE_LOG(LogTemp, Warning, TEXT("Starting find sessions"));
		SessionInterface->FindSessions(0, SessionSearch.ToSharedRef());
	}
}

void ULycanthropyGameInstance::OnFindSessionsComplete(bool Success) {
	if (Success && SessionSearch.IsValid() && Menu != nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("Finished find session"));

		TArray<FServerData> ServerNames;

		for (const FOnlineSessionSearchResult& SearchResult : SessionSearch->SearchResults)
		{
			UE_LOG(LogTemp, Warning, TEXT("Found session names: %s"), *SearchResult.GetSessionIdStr());
			FServerData Data;
			Data.MaxPlayers = SearchResult.Session.SessionSettings.NumPublicConnections;
			Data.CurrentPlayers = Data.MaxPlayers - SearchResult.Session.NumOpenPublicConnections;
			Data.HostUsername = SearchResult.Session.OwningUserName;
			FString ServerName;
			if (SearchResult.Session.SessionSettings.Get(SERVER_NAME_SETTINGS_KEY, ServerName))
			{
				Data.Name = ServerName;
			}
			else
			{
				Data.Name = "Could not find name.";
			}
			ServerNames.Add(Data);
		}

		Menu->SetServerList(ServerNames);
	}
}

void ULycanthropyGameInstance::CreateSession()
{
	if (SessionInterface.IsValid())
	{
		FOnlineSessionSettings SessionSettings;

		//If subsytem is null (Steam is not being used), enable LAN.
		if (IOnlineSubsystem::Get()->GetSubsystemName() == "NULL")
		{
			SessionSettings.bIsLANMatch = true;
		}
		//If Steam subsytem is enabled, disable LAN
		else
		{
			SessionSettings.bIsLANMatch = false;
		}
		SessionSettings.NumPublicConnections = 5;	//Number of max players
		SessionSettings.bShouldAdvertise = true;
		SessionSettings.bUsesPresence = true;	//Enables Steam lobby
		SessionSettings.Set(SERVER_NAME_SETTINGS_KEY, DesiredServerName, EOnlineDataAdvertisementType::ViaOnlineServiceAndPing);

		SessionInterface->CreateSession(0, SESSION_NAME, SessionSettings);
	}
}

void ULycanthropyGameInstance::Join(uint32 Index)
{
	if (!SessionInterface.IsValid()) return;
	if (!SessionSearch.IsValid()) return;

	//Teardown menu if it exists
	if (Menu != nullptr)
	{
		Menu->Teardown();
	}

	SessionInterface->JoinSession(0, SESSION_NAME, SessionSearch->SearchResults[Index]);

}

void ULycanthropyGameInstance::OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result)
{
	if (!SessionInterface.IsValid()) return;

	FString Address;
	if (!SessionInterface->GetResolvedConnectString(SessionName, Address))
	{
		UE_LOG(LogTemp, Warning, TEXT("Could not get connect string."));
		return;
	}

	//On-screen debug message
	UEngine* Engine = GetEngine();	//Get engine object
	if (!ensure(Engine != nullptr)) return;	//Protection from engine pointer being null causing crash

	Engine->AddOnScreenDebugMessage(0, 5, FColor::Green, FString::Printf(TEXT("Joining %s"), *Address));

	//Client travel (client's player controller moves to a server being hosted)
	APlayerController* PlayerController = GetFirstLocalPlayerController();
	if (!ensure(PlayerController != nullptr)) return;	//Protection from player controller pointer being null causing crash

	PlayerController->ClientTravel(Address, ETravelType::TRAVEL_Absolute);
}

void ULycanthropyGameInstance::StartSession()
{
	if (SessionInterface.IsValid())
	{
		SessionInterface->StartSession(SESSION_NAME);
	}
}

void ULycanthropyGameInstance::LoadMainMenu()
{
	APlayerController* PlayerController = GetFirstLocalPlayerController();
	if (!ensure(PlayerController != nullptr)) return;	//Protection from player controller pointer being null causing crash

	PlayerController->ClientTravel("/Game/MenuSystem/MainMenu", ETravelType::TRAVEL_Absolute);
}