// Copyright Epic Games, Inc. All Rights Reserved.

#include "LycanthropyCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Net/UnrealNetwork.h"
#include "Engine/Engine.h"
#include "Kismet/GameplayStatics.h"
#include "TimerManager.h"
#include "Particles/ParticleSystemComponent.h"

#include "LycanthropyGameInstance.h"
#include "Weapon.h"

//////////////////////////////////////////////////////////////////////////
// ALycanthropyCharacter

ALycanthropyCharacter::ALycanthropyCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;
	GetCharacterMovement()->MaxWalkSpeed = MAX_WALK_SPEED;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	//Initialize the player's Health
	MaxHealth = 100.0f;
	CurrentHealth = MaxHealth;

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
}

//////////////////////////////////////////////////////////////////////////
// Input

void ALycanthropyCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
	
	PlayerInputComponent->BindAction("OpenInGameMenu", IE_Pressed, this, &ALycanthropyCharacter::OpenInGameMenu); // Call In Game Menu
	
	PlayerInputComponent->BindAction("StartSprinting", IE_Pressed, this, &ALycanthropyCharacter::StartSprinting); // Bind sprinting to pressing shift
	PlayerInputComponent->BindAction("StopSprinting", IE_Released, this, &ALycanthropyCharacter::StopSprinting); // Bind stop sprinting to lifting off shift

	//PlayerInputComponent->BindAction("SetHeavyAttackTimer", IE_Pressed, this, &ALycanthropyCharacter::SetHeavyAttackTimer);	// Set timer that indicates a heavy attack when any attack button is pressed
	PlayerInputComponent->BindAction("LeftClawAttack", IE_Pressed, this, &ALycanthropyCharacter::LeftClawAttack); // Bind left claw attack to left mouse click
	PlayerInputComponent->BindAction("RightClawAttack", IE_Pressed, this, &ALycanthropyCharacter::RightClawAttack); // Bind right claw attack to right mouse click
	PlayerInputComponent->BindAction("BiteAttack", IE_Pressed, this, &ALycanthropyCharacter::BiteAttack); // Bind bite attack to middle mouse click

	PlayerInputComponent->BindAxis("MoveForward", this, &ALycanthropyCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ALycanthropyCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ALycanthropyCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ALycanthropyCharacter::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &ALycanthropyCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &ALycanthropyCharacter::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &ALycanthropyCharacter::OnResetVR);
}

void ALycanthropyCharacter::BeginPlay()
{
	Super::BeginPlay();

	//Set starting health
	CurrentHealth = MaxHealth;

	//Spawn default weapon
	//SpawnWeapons();

	//LeftClawWeapon = Cast<AWeapon>(GetMesh()->GetChildComponent(0));
	//RightClawWeapon = Cast<AWeapon>(GetMesh()->GetChildComponent(1));
	//BiteWeapon = Cast<AWeapon>(GetMesh()->GetChildComponent(2));

	if (GetLocalRole() == ROLE_Authority)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 10.0f, FColor::Red, TEXT("My role is Authority"));
	}
	if (GetLocalRole() != ROLE_Authority)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 10.0f, FColor::Yellow, TEXT("My role is Client"));
	}
}

void ALycanthropyCharacter::SpawnWeapons()
{
	//SpawnLeftClawWeapon
	LeftClawWeapon = GetWorld()->SpawnActor<AWeapon>(LeftClawWeaponClass);
	//Attach weapon mesh to socket
	LeftClawWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, TEXT("LeftClawSocket"));
	//Set spawning character as owner
	LeftClawWeapon->SetOwner(this);

	//Spawn Right Claw Weapon
	RightClawWeapon = GetWorld()->SpawnActor<AWeapon>(RightClawWeaponClass);
	//Attach weapon mesh to socket
	RightClawWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, TEXT("RightClawSocket"));
	//Set spawning character as owner
	RightClawWeapon->SetOwner(this);

	//Spawn Bite Weapon
	BiteWeapon = GetWorld()->SpawnActor<AWeapon>(BiteWeaponClass);
	//Attach weapon mesh to socket
	BiteWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, TEXT("HeadSocket"));
	//Set spawning character as owner
	BiteWeapon->SetOwner(this);
}

void ALycanthropyCharacter::ServerSetWeaponOwner_Implementation(AWeapon* ClientWeapon, ALycanthropyCharacter* NewOwner)
{
	if (GetLocalRole() == ROLE_Authority)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Blue, "Setting owner.");
		ClientWeapon->SetOwner(NewOwner);
	}
}

bool ALycanthropyCharacter::ServerSetWeaponOwner_Validate(AWeapon* ClientWeapon, ALycanthropyCharacter* NewOwner)
{
	return true;
}

void ALycanthropyCharacter::OnResetVR()
{
	// If Lycanthropy is added to a project via 'Add Feature' in the Unreal Editor the dependency on HeadMountedDisplay in Lycanthropy.Build.cs is not automatically propagated
	// and a linker error will result.
	// You will need to either:
	//		Add "HeadMountedDisplay" to [YourProject].Build.cs PublicDependencyModuleNames in order to build successfully (appropriate if supporting VR).
	// or:
	//		Comment or delete the call to ResetOrientationAndPosition below (appropriate if not supporting VR)
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void ALycanthropyCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
		Jump();
}

void ALycanthropyCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
		StopJumping();
}

void ALycanthropyCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ALycanthropyCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

//////////////////////////////////////////////////////////////////////////
// Replicated Properties

void ALycanthropyCharacter::GetLifetimeReplicatedProps(TArray <FLifetimeProperty> & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	//Replicate current health
	DOREPLIFETIME(ALycanthropyCharacter, CurrentHealth);

	//Replicate weapons
	DOREPLIFETIME(ALycanthropyCharacter, LeftClawWeapon);
	DOREPLIFETIME(ALycanthropyCharacter, RightClawWeapon);
	DOREPLIFETIME(ALycanthropyCharacter, BiteWeapon);
}

void ALycanthropyCharacter::MulticastSetWeaponOwner_Implementation(AWeapon* ClientWeapon, ALycanthropyCharacter* NewOwner)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Blue, "Setting owner.");
	ClientWeapon->SetOwner(NewOwner);
}

void ALycanthropyCharacter::MulticastPlayAnimMontage_Implementation(class UAnimMontage* AnimMontage, float InPlayRate, FName StartSectionName)
{
	PlayAnimMontage(AnimMontage, InPlayRate, StartSectionName);
}

void ALycanthropyCharacter::MulticastStopCurrentMontage_Implementation()
{
	StopAnimMontage(GetCurrentMontage());
}

void ALycanthropyCharacter::ServerPlayReplicatedSound_Implementation(bool b3DSound, USoundBase* SoundToPlay)
{
	MulticastPlayReplicatedSound(b3DSound, SoundToPlay);
}

bool ALycanthropyCharacter::ServerPlayReplicatedSound_Validate(bool b3DSound, USoundBase* SoundToPlay)
{
	return true;
}

void ALycanthropyCharacter::MulticastPlayReplicatedSound_Implementation(bool b3DSound, USoundBase* SoundToPlay)
{
	UGameplayStatics::PlaySound2D(GetWorld(), SoundToPlay, .25, 1, 0, nullptr, false, false);
}

void ALycanthropyCharacter::MulticastSpawnReplicatedSound_Implementation(bool b3DSound, USoundBase* SoundToPlay)
{
	MusicComponent = UGameplayStatics::SpawnSound2D(GetWorld(), SoundToPlay, .30, 1, 0, nullptr, false, false);
}

void ALycanthropyCharacter::MulticastDespawnReplicatedSound_Implementation(bool b3DSound, USoundBase* SoundToPlay)
{
	
}

void ALycanthropyCharacter::MulticastPlayDeathRagdoll_Implementation()
{
	GetMesh()->SetCollisionProfileName("Ragdoll");
	GetMesh()->SetSimulatePhysics(true);
}

void ALycanthropyCharacter::OnHealthUpdate()
{
	//Client-specific functionality
	if (IsLocallyControlled())
	{
		FString healthMessage = FString::Printf(TEXT("You now have %f health remainging."), CurrentHealth);
		//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Blue, healthMessage);

		if (CurrentHealth <= 0)
		{
			FString deathMessage = FString::Printf(TEXT("You have been killed."));
			//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, deathMessage);
		}
	}

	//Server Specific Functionality
	if (GetLocalRole() == ROLE_Authority)
	{
		FString healthMessage = FString::Printf(TEXT("%s now has %f health remaining."), *GetFName().ToString(), CurrentHealth);
		//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Blue, healthMessage);
	}

	//Functions that occur on all machines. 
	/*
		Any special functionality that should occur as a result of damage or death should be placed here.
	*/
}

void ALycanthropyCharacter::OnRep_CurrentHealth()
{
	OnHealthUpdate();
}

void ALycanthropyCharacter::SetCurrentHealth(float healthValue)
{
	if (GetLocalRole() == ROLE_Authority)
	{
		CurrentHealth = FMath::Clamp(healthValue, 0.0f, MaxHealth);
		OnHealthUpdate();
	}
}

float ALycanthropyCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	float DamageToApply = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);	//Call parent implementation of method first, as to not completely override its functionality
	DamageToApply = FMath::Min(CurrentHealth, DamageToApply);	//Do not allow health to fall below 0
	CurrentHealth -= DamageToApply;

	UE_LOG(LogTemp, Warning, TEXT("Health left %F"), CurrentHealth);

	/*
	//Death handling
	if (IsDead())
	{
		if (CharacterType == "Vampire")
		{
			//Cast game instance to lycanthropy game instance and cast event instigator controller to player controller
			ULycanthropyGameInstance* GameInstance = Cast<ULycanthropyGameInstance>(this->GetGameInstance());
			//if (GameInstance == nullptr) { return; }

			APlayerController* Attacker = Cast<APlayerController>(EventInstigator);
			//if (Instigator == nullptr) { return; }

			//Increment the kill counter in the proper slot for the given player controller
			GameInstance->KillCount[UGameplayStatics::GetPlayerControllerID(Attacker)]++;
		}
	}
	*/
	
	//Deliver knockback
	Knockback();

	return DamageToApply;
}

bool ALycanthropyCharacter::IsDead() const
{
	return CurrentHealth <= 0;
}

//Knockback handling
void ALycanthropyCharacter::Knockback()
{
	/*
	if (CharacterType == "Vampire" && SecondaryType != "Tank")
	{
		MulticastPlayAnimMontage(FrontKnockbackMontage, 1.0f, "Default");
	}
	else
	{
		MulticastPlayAnimMontage(FrontHitMontage, 1.0f, "Default");
	}
	*/

	if (CharacterType == "Vampire")
	{
		MulticastPlayAnimMontage(FrontHitMontage, 1.0f, "Default");
	}
}

void ALycanthropyCharacter::MoveForward(float Value)
{
	if ((Controller != nullptr) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void ALycanthropyCharacter::MoveRight(float Value)
{
	if ( (Controller != nullptr) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

//Opens in-game menu
void ALycanthropyCharacter::OpenInGameMenu()
{
	ULycanthropyGameInstance* GameInstance = Cast<ULycanthropyGameInstance>(this->GetGameInstance());
	if (!ensure(GameInstance != nullptr)) return;
	GameInstance->InGameLoadMenu();
}

//Increases max walk speed
void ALycanthropyCharacter::StartSprinting()
{
	if (GetLocalRole() != ROLE_Authority)
	{
		ServerStartSprinting();
	}

	//Run on both server and client. Otherwise, the client's speed changes, but the sprinting animation jitters.
	GetCharacterMovement()->MaxWalkSpeed = MAX_SPRINT_SPEED;
}

//Return to regular max speed
void ALycanthropyCharacter::StopSprinting()
{
	if (GetLocalRole() != ROLE_Authority)
	{
		ServerStopSprinting();
	}
	
	//Run on both server and client. Otherwise, the client's speed changes, but the sprinting animation jitters.
	GetCharacterMovement()->MaxWalkSpeed = MAX_WALK_SPEED;
}

//Increase client max speed
void ALycanthropyCharacter::ServerStartSprinting_Implementation()
{
	StartSprinting();
}

//Return to regular client max speed
void ALycanthropyCharacter::ServerStopSprinting_Implementation()
{
	StopSprinting();
}

//Set timer that determines whether or not an attack is a heavy attack
void ALycanthropyCharacter::SetHeavyAttackTimer()
{
	bIsHeavyAttacking = false;	//Reset heavy attacking boolean to default false
	GetWorldTimerManager().SetTimer(HeavyAttackTimer, this, &ALycanthropyCharacter::SetIsHeavyAttacking, 1);	//Set heavy attacking boolean to true if button held for 1 second
}

void ALycanthropyCharacter::SetIsHeavyAttacking()
{
	bIsHeavyAttacking = true;
}

void ALycanthropyCharacter::LeftClawAttack()
{
	//Check client for nullpoiners and incorrect authority. Escape if either is detected.
	if (GetLocalRole() != ROLE_Authority)
	{
		if (LeftClawWeapon == nullptr)
		{
			//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::White, TEXT("Left Claw Weapon is nullptr"));
			return;
		}
		if (LeftClawWeapon->GetLocalRole() == ROLE_Authority)
		{
			//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::White, TEXT("Left Claw Role mismatch"));
			return;
		}
	}

	//If no weapon is currently swinging
	if (LeftClawWeapon != nullptr && RightClawWeapon != nullptr && BiteWeapon != nullptr)
	{
		if (LeftClawWeapon->CanSwingWeapon() && RightClawWeapon->CanSwingWeapon() && BiteWeapon->CanSwingWeapon())
		{
			LeftClawWeapon->Attack();
		}
	}
}

void ALycanthropyCharacter::RightClawAttack()
{
	//Check client for nullpoiners and incorrect authority. Escape if either is detected.
	if (GetLocalRole() != ROLE_Authority)
	{
		if (RightClawWeapon == nullptr)
		{
			//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::White, TEXT("Right Claw Weapon is nullptr"));
			return;
		}
		if (RightClawWeapon->GetLocalRole() == ROLE_Authority)
		{
			//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::White, TEXT("Right Claw Role mismatch"));
			return;
		}
	}

	//If no weapon is currently swinging
	if (LeftClawWeapon != nullptr && RightClawWeapon != nullptr && BiteWeapon != nullptr)
	{
		if (LeftClawWeapon->CanSwingWeapon() && RightClawWeapon->CanSwingWeapon() && BiteWeapon->CanSwingWeapon())
		{
			RightClawWeapon->Attack();
		}
	}
}

void ALycanthropyCharacter::BiteAttack()
{
	//Check client for nullpoiners and incorrect authority. Escape if either is detected.
	if (GetLocalRole() != ROLE_Authority)
	{
		if (BiteWeapon == nullptr)
		{
			//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::White, TEXT("Bite Weapon is nullptr"));
			return;
		}
		if (BiteWeapon->GetLocalRole() == ROLE_Authority)
		{
			//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::White, TEXT("Bite Role mismatch"));
			return;
		}
	}

	//If no weapon is currently swinging
	if (LeftClawWeapon != nullptr && RightClawWeapon != nullptr && BiteWeapon != nullptr)
	{
		if (LeftClawWeapon->CanSwingWeapon() && RightClawWeapon->CanSwingWeapon() && BiteWeapon->CanSwingWeapon())
		{
			BiteWeapon->Attack();
		}
	}
}