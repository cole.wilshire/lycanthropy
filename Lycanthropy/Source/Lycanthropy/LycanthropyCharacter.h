// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TimerManager.h"

#include "weapon.h"
#include "MenuSystem/UIWidget.h"

#include "LycanthropyCharacter.generated.h"

class AWeapon;

UCLASS(config=Game)
class ALycanthropyCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;


public:
	ALycanthropyCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

	/** Property replication for multiplayer*/
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	
	UFUNCTION(NetMulticast, Reliable)
	void MulticastSetWeaponOwner(AWeapon* ClientWeapon, ALycanthropyCharacter* NewOwner);

	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void MulticastPlayAnimMontage(class UAnimMontage* AnimMontage, float InPlayRate, FName StartSectionName);

	UFUNCTION(NetMulticast, Reliable)
	void MulticastStopCurrentMontage();

	UFUNCTION(NetMulticast, Reliable)
	void MulticastPlayDeathRagdoll();

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerPlayReplicatedSound(bool b3DSound, USoundBase* SoundToPlay);

	UFUNCTION(NetMulticast, Unreliable, BlueprintCallable)
	void MulticastPlayReplicatedSound(bool b3DSound, USoundBase* SoundToPlay);

	UFUNCTION(NetMulticast, Unreliable, BlueprintCallable)
	void MulticastSpawnReplicatedSound(bool b3DSound, USoundBase* SoundToPlay);

	UFUNCTION(NetMulticast, Unreliable, BlueprintCallable)
	void MulticastDespawnReplicatedSound(bool b3DSound, USoundBase* SoundToPlay);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UAudioComponent* MusicComponent = nullptr;

	/** Response to health being updated. Called on the server immediately after modification, and on clients in response to a RepNotify*/
	void OnHealthUpdate();

	//Getter for max health
	UFUNCTION(BlueprintPure, Category="Health")
	FORCEINLINE float GetMaxHealth() const { return CurrentHealth; }

	//Setter for Current Health. Clamps value between 0 and MaxHealth, and calls OnHealthUpdate. Should only be called on server
	UFUNCTION(BlueprintCallable, Category = "Health")
	void SetCurrentHealth(float healthValue);

	//Spawn weapons
	void SpawnWeapons();

	//PRC call from client to ask server to change weapon owner to client
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerSetWeaponOwner(AWeapon* ClientWeapon, ALycanthropyCharacter* NewOwner);

	//Attack methods
	void SetHeavyAttackTimer();
	void SetIsHeavyAttacking();
	bool bIsHeavyAttacking = 0;
	void LeftClawAttack();
	void RightClawAttack();
	void BiteAttack();

	//Damage-handling
	UFUNCTION(BlueprintCallable, Category = "Health")
	float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController *EventInstigator, AActor *DamageCauser) override;

	//Tells whether or not a character is dead. A pure blueprint node has no execution pin
	UFUNCTION(BlueprintPure)
	bool IsDead() const;

	//Knockback Handling
	UFUNCTION(BlueprintCallable)
	void Knockback();

	UPROPERTY(Replicated, BlueprintReadWrite, Category = Player)
	AWeapon* LeftClawWeapon;

	UPROPERTY(Replicated, BlueprintReadWrite, Category = Player)
	AWeapon* RightClawWeapon;

	UPROPERTY(Replicated, BlueprintReadWrite, Category = Player)
	AWeapon* BiteWeapon;

	//Character Typea, for determining ally relationships
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Player)
	FString CharacterType;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Player)
	FString SecondaryType;

	//Kill Counter
	int KillCount = 0;

	//Last enemy character that damaged character
	ALycanthropyCharacter* LastContact;

	//Player's UI Object
	UUIWidget* UserInterface = nullptr;

	//Montage that is played when vampires are hit by standard attacks from the front
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Animations)
	class UAnimMontage* FrontHitMontage;

	//Montage that is played when vampires are hit by standard attacks from the back
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Animations)
	class UAnimMontage* BackHitMontage;

	//Montage that is played when vampires are hit by heavy attacks from the back
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Animations)
	class UAnimMontage* FrontKnockbackMontage;

	//Montage that is played when vampires are hit by heavy attacks from the back
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Animations)
	class UAnimMontage* BackKnockbackMontage;

	//UPROPERTY(EditAnywhere, BlueprintReadOnly)
	//FVector ImpactLocation;

protected:
	//Max non-sprint speed
	const float MAX_WALK_SPEED = 600.0f;

	//Max sprint speed
	const float MAX_SPRINT_SPEED = MAX_WALK_SPEED * 1.5;

	//Call when character is spawned
	void BeginPlay() override;

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

	//Opens in-game menu
	void OpenInGameMenu();

	//Increase max walk speed
	void StartSprinting();

	//Set max walk speed to default
	void StopSprinting();

	//Increase max walk speed for clients
	UFUNCTION(Server, Reliable)
	void ServerStartSprinting();

	//Set max walk speed to default for clients
	UFUNCTION(Server, Reliable)
	void ServerStopSprinting();

	//Max health
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Health")
	float MaxHealth;

	//Max health
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Health")
	float MaxSpecial;

	//Current Health
	UPROPERTY(ReplicatedUsing = OnRep_CurrentHealth, EditAnywhere, BlueprintReadOnly)
	float CurrentHealth;

	//Current Special
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float CurrentSpecial = 0;

	//RepNotify for changes made to current health
	UFUNCTION()
	void OnRep_CurrentHealth();

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

private:
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AWeapon> LeftClawWeaponClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AWeapon> RightClawWeaponClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AWeapon> BiteWeaponClass;

	FTimerHandle HeavyAttackTimer;
};

