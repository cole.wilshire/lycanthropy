// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "OnlineSubsystem.h"
#include "Interfaces/OnlineSessionInterface.h"

#include "MenuSystem/MenuInterface.h"
#include "MenuSystem/UIInterface.h"
#include "LycanthropyGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class LYCANTHROPY_API ULycanthropyGameInstance : public UGameInstance, public IMenuInterface
{
	GENERATED_BODY()
	
public:
	ULycanthropyGameInstance(const FObjectInitializer& ObjectInitializer);

	virtual void Init();

	//Functions callable in a Blueprint
	UFUNCTION(BlueprintCallable)
	void LoadMenuWidget();

	UFUNCTION(BlueprintCallable)
	void InGameLoadMenu();

	UFUNCTION(BlueprintCallable)
	void EndgameLoadMenu();

	UFUNCTION(BlueprintCallable)
	void EndgameLoadMenuDefeat();

	UFUNCTION(BlueprintCallable)
	void ShowUI();

	UFUNCTION(BlueprintCallable)
	void KillUI();

	//Console commands (Exec)
	UFUNCTION(Exec)
	void Host(FString ServerName) override;

	UFUNCTION(Exec)
	void Join(uint32 Index) override;

	void StartSession();

	UFUNCTION(BlueprintCallable)
	virtual void LoadMainMenu() override;

	void RefreshServerList() override;

	uint32 LobbySize = 1;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	int NumBarrierVampires = 0;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	int NumPlayers = 0;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	TArray<int32> KillCount;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	bool MusicOn = 0;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	bool PlaythroughIsSucess = 0;

private:
	TSubclassOf<class UUserWidget> MenuClass;	//UUserWidget requires adding (UMG Unreal Motion Graphics) in Lycanthropy.Build.cs to PublicDependencyModuleNames. See Section 2, video 3 (video 26) for help.
	TSubclassOf<class UUserWidget> InGameMenuClass;
	TSubclassOf<class UUserWidget> EndgameMenuClass;
	TSubclassOf<class UUserWidget> EndgameMenuDefeatClass;
	TSubclassOf<class UUserWidget> UIClass;

	class UMainMenu* Menu;

	IOnlineSessionPtr SessionInterface;
	TSharedPtr<class FOnlineSessionSearch> SessionSearch;

	//Delegates
	void OnCreateSessionComplete(FName SessionName, bool Success);
	void OnDestroySessionComplete(FName SessionName, bool Success);
	void OnFindSessionsComplete(bool Success);
	void OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result);
	void OnNetworkFailure(UWorld* World, UNetDriver* NetDriver, ENetworkFailure::Type FailureType, const FString& ErrorString);

	FString DesiredServerName;
	void CreateSession();
};
