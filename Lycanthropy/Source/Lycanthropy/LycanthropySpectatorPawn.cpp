// Fill out your copyright notice in the Description page of Project Settings.


#include "LycanthropySpectatorPawn.h"

#include "LycanthropyGameInstance.h"

void ALycanthropySpectatorPawn::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Set up gameplay key bindings
	PlayerInputComponent->BindAction("OpenInGameMenu", IE_Pressed, this, &ALycanthropySpectatorPawn::OpenInGameMenu); // Call In Game Menu
}

//Opens in-game menu
void ALycanthropySpectatorPawn::OpenInGameMenu()
{
	ULycanthropyGameInstance* GameInstance = Cast<ULycanthropyGameInstance>(this->GetGameInstance());
	if (!ensure(GameInstance != nullptr)) return;
	GameInstance->InGameLoadMenu();
}