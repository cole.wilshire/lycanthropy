// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_Attack.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AIController.h"

#include "LycanthropyCharacter.h"

UBTTask_Attack::UBTTask_Attack()
{
	NodeName = TEXT("Attack");
}

EBTNodeResult::Type UBTTask_Attack::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Super::ExecuteTask(OwnerComp, NodeMemory);

	if (OwnerComp.GetAIOwner() == nullptr)
	{
		return EBTNodeResult::Failed;
	}

	ALycanthropyCharacter* Character = Cast<ALycanthropyCharacter>(OwnerComp.GetAIOwner()->GetPawn());
	if (Character == nullptr)
	{
		return EBTNodeResult::Failed;
	}

	//Choose a random attack type to use
	int RandomNum = rand() % 3;
	if (RandomNum == 0)
	{
		Character->LeftClawAttack();
	}
	else if (RandomNum == 1)
	{
		Character->RightClawAttack();
	}
	else if (RandomNum == 2)
	{
		Character->BiteAttack();
	}

	return EBTNodeResult::Succeeded;	//Node finished successfully
}