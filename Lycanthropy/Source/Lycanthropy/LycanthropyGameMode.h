// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "LycanthropyGameMode.generated.h"

UCLASS(minimalapi)
class ALycanthropyGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ALycanthropyGameMode();
	
	//Reassign authority of weapons to the character that spawned them
	void ReparentWeapons();

protected:
	virtual void BeginPlay() override;
};



