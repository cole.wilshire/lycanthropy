// Fill out your copyright notice in the Description page of Project Settings.


#include "EndgameMenuDefeat.h"
#include "Components/Button.h"
#include "Components/WidgetSwitcher.h"
#include "Components/EditableTextBox.h"
#include "Components/TextBlock.h"
#include "../LycanthropyGameInstance.h"

bool UEndgameMenuDefeat::Initialize()
{
	bool Success = Super::Initialize();
	if (!Success) return false;

	//Bind exit button in main menu Blueprint to trigger CancelPressed
	if (ExitButton == nullptr) { return false; }
	ExitButton->OnClicked.AddDynamic(this, &UEndgameMenuDefeat::ExitPressed);

	return true;
}

void UEndgameMenuDefeat::ExitPressed()
{
	if (MenuInterface != nullptr)
	{
		Teardown();
		MenuInterface->LoadMainMenu();
	}
}
