// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "UIInterface.h"

#include "UIWidget.generated.h"

/**
 * 
 */
UCLASS()
class LYCANTHROPY_API UUIWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	void Setup();
	void Teardown();

	void SetUIInterface(IUIInterface* UIInterface);

protected:
	IUIInterface* UIInterface;
};
