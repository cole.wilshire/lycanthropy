// Fill out your copyright notice in the Description page of Project Settings.


#include "MenuWidget.h"

void UMenuWidget::Setup()
{
	//Add widget to viewport over the top of what's already there
	this->AddToViewport();

	//Get world pointer and make sure it's not null
	UWorld* World = GetWorld();
	if (!ensure(World != nullptr)) return;

	//Get player controller
	APlayerController* PlayerController = World->GetFirstPlayerController();
	if (!ensure(PlayerController != nullptr)) return;	//Protection from player controller pointer being null causing crash

	//Mouse functionality
	FInputModeUIOnly InputModeData;
	InputModeData.SetWidgetToFocus(this->TakeWidget());	//Choose which widget to focus. Takes in an SWidget
	InputModeData.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);	//Don't lock mouse into viewport

	PlayerController->SetInputMode(InputModeData);

	PlayerController->bShowMouseCursor = true;	//Show mouse cursor in-game
}

void UMenuWidget::SetMenuInterface(IMenuInterface* Interface)
{
	this->MenuInterface = Interface;
}

void UMenuWidget::Teardown()
{
	//Remove menu from viewport
	this->RemoveFromViewport();

	//Get world pointer and make sure it's not null
	UWorld* World = GetWorld();
	if (!ensure(World != nullptr)) return;

	//Get player controller
	APlayerController* PlayerController = World->GetFirstPlayerController();
	if (!ensure(PlayerController != nullptr)) return;	//Protection from player controller pointer

	//Restore control to player character from the menu
	FInputModeGameOnly InputModeData;
	PlayerController->SetInputMode(InputModeData);
	PlayerController->bShowMouseCursor = false;
}
