// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MenuWidget.h"
#include "EndgameMenuDefeat.generated.h"

/**
 * 
 */
UCLASS()
class LYCANTHROPY_API UEndgameMenuDefeat : public UMenuWidget
{
	GENERATED_BODY()
	
protected:
	virtual bool Initialize() override;

private:
	UPROPERTY(meta = (BindWidget))
	class UButton* ExitButton;

	UFUNCTION()
	void ExitPressed();
};
