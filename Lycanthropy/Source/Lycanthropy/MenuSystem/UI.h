// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UIWidget.h"
#include "UI.generated.h"

/**
 * 
 */
UCLASS()
class LYCANTHROPY_API UUI : public UUIWidget
{
	GENERATED_BODY()
	
protected:
	virtual bool Initialize() override;
};
