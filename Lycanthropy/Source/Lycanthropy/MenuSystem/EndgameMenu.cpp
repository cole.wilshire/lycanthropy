// Fill out your copyright notice in the Description page of Project Settings.


#include "EndgameMenu.h"
#include "Components/Button.h"
#include "Components/WidgetSwitcher.h"
#include "Components/EditableTextBox.h"
#include "Components/TextBlock.h"
#include "../LycanthropyGameInstance.h"

bool UEndgameMenu::Initialize()
{
	bool Success = Super::Initialize();
	if (!Success) return false;

	//Bind exit button in main menu Blueprint to trigger CancelPressed
	if (ExitButton == nullptr) { return false; }
	ExitButton->OnClicked.AddDynamic(this, &UEndgameMenu::ExitPressed);

	return true;
}

void UEndgameMenu::ExitPressed()
{
	if (MenuInterface != nullptr)
	{
		Teardown();
		MenuInterface->LoadMainMenu();
	}
}
