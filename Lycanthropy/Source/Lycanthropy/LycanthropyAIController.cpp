// Fill out your copyright notice in the Description page of Project Settings.


#include "LycanthropyAIController.h"

#include "Kismet/GameplayStatics.h"
#include "BehaviorTree/BlackboardComponent.h"

void ALycanthropyAIController::BeginPlay()
{
	Super::BeginPlay();

	if (GetLocalRole() == ROLE_Authority)
	{
		if (AIBehavior != nullptr)
		{
			RunBehaviorTree(AIBehavior);
		}
	}
}

void ALycanthropyAIController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}