// Copyright Epic Games, Inc. All Rights Reserved.

#include "LycanthropyGameMode.h"
#include "LycanthropyCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"

ALycanthropyGameMode::ALycanthropyGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/WerewolfCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}

void ALycanthropyGameMode::BeginPlay()
{
	Super::BeginPlay();

	//ReparentWeapons();
}

//void AL

//Reassign authority of weapons to the parents that spawned them
void ALycanthropyGameMode::ReparentWeapons()
{
	//If role is server
	if (GetLocalRole() == ROLE_Authority)
	{
		//Get array of Lycanthropy Characters
		TArray<AActor*> ActorArray;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), ALycanthropyCharacter::StaticClass(), ActorArray);

		//Iterate through array of Lcanthropy Characters
		for (int i = 0; i < ActorArray.Num(); ++i)
		{
			//Rechild their Weapon, if it exists
			ALycanthropyCharacter* WeaponOwner = Cast<ALycanthropyCharacter>(ActorArray[i]);

			//if (WeaponOwner->ActiveWeapon != nullptr)
			//{
			//if (WeaponOwner->ActiveWeapon != nullptr)
			//{
			//	WeaponOwner->ActiveWeapon->SetOwner(WeaponOwner);
			//}
			if (WeaponOwner->LeftClawWeapon != nullptr)
			{
				WeaponOwner->LeftClawWeapon->SetOwner(WeaponOwner);
			}
			if (WeaponOwner->RightClawWeapon != nullptr)
			{
				WeaponOwner->RightClawWeapon->SetOwner(WeaponOwner);
			}
			if (WeaponOwner->BiteWeapon != nullptr)
			{
				WeaponOwner->BiteWeapon->SetOwner(WeaponOwner);
			}
			//}
		}
	}
}
