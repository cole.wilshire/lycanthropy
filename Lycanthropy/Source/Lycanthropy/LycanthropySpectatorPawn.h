// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SpectatorPawn.h"

#include "LycanthropyCharacter.h"

#include "LycanthropySpectatorPawn.generated.h"

/**
 * 
 */
UCLASS()
class LYCANTHROPY_API ALycanthropySpectatorPawn : public ASpectatorPawn
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int Kills = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ALycanthropyCharacter* FormerWerewolfCharacter = nullptr;

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

	//Opens in-game menu
	void OpenInGameMenu();
};
