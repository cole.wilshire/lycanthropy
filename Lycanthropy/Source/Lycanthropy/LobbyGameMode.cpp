// Fill out your copyright notice in the Description page of Project Settings.


#include "LobbyGameMode.h"

#include "LycanthropyGameInstance.h"

void ALobbyGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);

	++NumberOfPlayers;

	//Trigger start game 5 seconds after the specified number of players join
	ULycanthropyGameInstance* GameInstance = Cast<ULycanthropyGameInstance>(GetGameInstance());
	if (GameInstance == nullptr) return;

	uint32 LobbySize = GameInstance->LobbySize;

	if (GameInstance->LobbySize <= 1)
	{
		LobbySize = 1;
	}
	else if (GameInstance->LobbySize >= 5)
	{
		LobbySize = 5;
	}
	else if (LobbySize > 1 && LobbySize < 5)
	{

	}
	else
	{
		LobbySize = 1;	//Default to 1 player lobby in case nothing or garbage is entered into number of players box
	}

	if (NumberOfPlayers >= LobbySize)
	{
		GetWorldTimerManager().SetTimer(GameStartTimer, this, &ALobbyGameMode::StartGame, 5);
	}
}

void ALobbyGameMode::Logout(AController* Exiting)
{
	Super::Logout(Exiting);

	--NumberOfPlayers;
}

void ALobbyGameMode::StartGame()
{
	ULycanthropyGameInstance* GameInstance = Cast<ULycanthropyGameInstance>(GetGameInstance());

	if (GameInstance == nullptr) return;

	GameInstance->StartSession();

	UWorld* World = GetWorld();
	if (!ensure(World != nullptr)) return;

	bUseSeamlessTravel = true;	//Enables a loading screen-like effect to make travel transition smoother
	World->ServerTravel("/Game/Maps/ThirdPersonExampleMap?listen");
}
