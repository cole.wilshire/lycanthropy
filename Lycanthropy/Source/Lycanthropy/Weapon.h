// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SkeletalMeshComponent.h"
#include "Weapon.generated.h"

//Weapon states
UENUM(BlueprintType)
enum class EWeaponState : uint8
{
	WS_Swinging,
	WS_Reversal,
	WS_HeavySwing,
	WS_Blocking,
	WS_BlockBreak,
	WS_None
};

UENUM(BlueprintType)
enum EFXTypes
{
	FXT_Ricochet,
	FXT_None,
	FXT_HitSword,
	FXT_HitAbsorb,
	FXT_HitWorld,
	FXT_HitFlesh
};

// Contains information of a single hitscan weapon linetrace
USTRUCT()
struct FHitTraceInfo
{
	GENERATED_BODY()

public:

	//UPROPERTY()
	//TEnumAsByte<EPhysicalSurface> SurfaceType;

	UPROPERTY()
	TEnumAsByte<EFXTypes> FXToPlay;

	UPROPERTY()
	FVector_NetQuantize HitLocation;
};

UCLASS()
class LYCANTHROPY_API AWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeapon();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Animations)
	class UAnimMontage* WeaponSwingsMontage;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Animations)
	class UAnimMontage* WeaponEquipMontage;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Animations)
	class UAnimMontage* WeaponBlockMontage;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Animations)
	class UAnimMontage* WeaponHeavySwingMontage;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Animations)
	class UAnimMontage* RicochetMontage;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = FX)
	class UParticleSystem* ImpactFlesh;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = FX)
	class UParticleSystem* ImpactWorldFX;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = FX)
	class UParticleSystem* ImpactSwordFX;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = FX)
	class USoundCue* ImpactFleshSound;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = FX)
	class USoundCue* BlockStartSound;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = FX)
	class USoundCue* BlockStopSound;

	/*Sound to play when we get hit while blocking*/
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = FX)
	class USoundCue* BlockAbsorbSound;

	/*Sound to play whatever our weapon hits world-geometry*/
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = FX)
	class USoundCue* RicochetSound;

	/*Sound to play as soon as we begin swinging*/
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = FX)
	class USoundCue* SwordSwingSound;

	/*Sound to play when our sword hits another sword*/
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = FX)
	class USoundCue* SwordClashSound;

	//Attack
	void Attack();

	/*Called at the end of the swing animation montage, Stops the tracing process and empties the target list*/
	UFUNCTION(BlueprintCallable, Category = Weapon)
	void AttackEnd();

	//Server-Sided function, begins the weapon swing logic ( animation, tracing, etc )
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerAttack();

	//Deal damage
	void DealDamage(APawn* DamageInstigator, AActor* ActorToDmg, float Dmg, FHitResult HitInfo);

	//Called by client to make a call on the server to deal damage to an actor
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerDealDamage(APawn* DamageInstigator, AActor* ActorToDmg, float Dmg, FHitResult HitInfo);

	void PlayImpactEffects(EFXTypes FXToPlay, FVector ImpactLoc);

	//Check to see if an enemy has been hurt yet during this attack
	bool CheckHurtEnemies(ACharacter* HurtChar);

	//Check to see if an actor has been hurt yet during this attack
	bool CheckHurtActors(AActor* HurtActor);

	/*Holds the current state of the weapon ( Swinging, Reversal, Heavy Swing, Blocking etc)*/
	UPROPERTY(Replicated, BlueprintReadOnly)
	EWeaponState WeaponState;

	//Used to change state of weapon
	void SetWeaponState(EWeaponState NewWeaponState);

	//Whether or not character can swing weapon
	bool CanSwingWeapon();

	/*Damage type to use when dealing damage*/
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Weapon)
	TSubclassOf<UDamageType> DamageType;

	/* Normal Swing damage*/
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Weapon)
	float NormalSwingDamage;

	/*Heavy swing damage*/
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Weapon)
	float HeavySwingDamage;

	/*amount of traces to perform each swing*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Weapon)
	float TracesPerSwing;

	//Whether or not tracing should be performed
	UPROPERTY(BlueprintReadWrite, Category = Weapon)
	bool bPerformTracing;

	// toggles tracing on and off
	UFUNCTION(BlueprintCallable, Category = Weapon)
	void ToggleTracing();

	//Whether or not actor is currently attacking
	UPROPERTY(Replicated, BlueprintReadOnly)
	bool bCurrentlySwinging = false;

	//Overlap handling
	UFUNCTION()
	void OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Traces)
	FName SocketStartName;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Traces)
	FName SocketEndName;

	FVector TraceStart;
	FVector TraceEnd;
	FVector StartSockEnd;
	FVector EndSockEnd;
	FVector LastFrameStartSockEnd;
	FVector LastFrameEndSockEnd;

	UPROPERTY()
	TArray<ACharacter*> EnemiesHitOnThisSwing;

	UPROPERTY()
	TArray<AActor*> ActorsHitOnThisSwing;

	bool IsSwinging();

	UFUNCTION(BlueprintImplementableEvent, Category = Weapon)
	void OnHit(FHitResult HitInf);

	UFUNCTION(Client, Reliable, WithValidation)
	void ClientPlayImpactFX(FHitTraceInfo HitInfo);

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UPROPERTY(VisibleAnywhere, Category = Weapon)
	USceneComponent* Root;

	UPROPERTY(VisibleAnywhere, Category = Weapon)
	USkeletalMeshComponent* Mesh;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(ReplicatedUsing = OnRep_HitTrace)
	FHitTraceInfo HitTraceInfo;

	UFUNCTION()
	void OnRep_HitTrace();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
