// Fill out your copyright notice in the Description page of Project Settings.


#include "BTService_PlayerLocation.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/Pawn.h"

#include "AIController.h"
#include "LycanthropyCharacter.h"

UBTService_PlayerLocation::UBTService_PlayerLocation()
{
	NodeName = TEXT("Update Player Location");

	//ConstructorHelpers::FClassFinder<UUserWidget> WerewolfCharacterBPClass(TEXT("/Game/Blueprint/BP_WerewolfCharacter"));
	//if (!ensure(WerewolfCharacterBPClass.Class != nullptr)) return;
}

void UBTService_PlayerLocation::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);

	//
	//Get array of Lycanthropy Characters
	TArray<AActor*> ActorArray;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ALycanthropyCharacter::StaticClass(), ActorArray);

	//Iterate through array of Lycanthropy Characters
	float ShortestDistance = 0;
	ALycanthropyCharacter* ClosestTarget = nullptr;
	float CurrentDistance = 0;
	ALycanthropyCharacter* PotentialTarget = nullptr;
	float DistanceToTarget = 0;
	APawn* TargetPawn = nullptr;

	for (int i = 0; i < ActorArray.Num(); ++i)
	{
		ALycanthropyCharacter* Character = Cast<ALycanthropyCharacter>(ActorArray[i]);
		if (Character == nullptr) return;

		if (Character->CharacterType == "Werewolf" && !Character->IsDead())
		{
			//Vector distance = abs(attackerLocation - Character->GetActorLocation());

			AAIController* AIController = OwnerComp.GetAIOwner();
			if (AIController == nullptr) { return; }
			APawn* AIOwner = AIController->GetPawn();
			if (AIOwner == nullptr) { return; }

			ALycanthropyCharacter* AttackingVampire = Cast<ALycanthropyCharacter>(AIOwner);
			if (AttackingVampire == nullptr) { return; }

			FVector DistanceVectorToTarget = AttackingVampire->GetActorLocation() - Character->GetActorLocation();
			DistanceToTarget = sqrtf(pow(DistanceVectorToTarget.X, 2) + pow(DistanceVectorToTarget.Y, 2) + pow(DistanceVectorToTarget.Z, 2));


			if (DistanceToTarget < ShortestDistance || ShortestDistance == 0)
			{
				ShortestDistance = DistanceToTarget;
				TargetPawn = Character;
			}
		}
	}

	//Return early if TargetPawn is nullptr, as this causes a crash when the last werewolf character dies otherwise
	if (TargetPawn == nullptr) { return; }

	OwnerComp.GetBlackboardComponent()->SetValueAsVector(GetSelectedBlackboardKey(), TargetPawn->GetActorLocation());
}